﻿using DataMarketFE.APIs;
using DataMarketFE.Settings;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.IdentityModel.Tokens.Jwt;

namespace DataMarketFE
{
    public class Startup
    {
        public bool IsPROD = false;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            IsPROD = env.IsProduction();

            var loggerconfig = new LoggerConfiguration()
            .MinimumLevel.Information()
            .Enrich.FromLogContext()
            .WriteTo.RollingFile("../Logs/DataMArketFE/" + env.EnvironmentName + "/log-{Date}.txt");

            if (env.IsProduction())
            {
                loggerconfig.WriteTo.Logentries("9dae0b5f-e5a4-4ff5-9a0a-fba2f71d4b55");
            }

            Log.Logger = loggerconfig.CreateLogger();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession(opts =>
            {
                opts.Cookie = new CookieBuilder
                {
                    Name = ".AspNetCore.Session"
                };

                opts.IdleTimeout = TimeSpan.FromDays(1);
            });

            if (IsPROD)
            {
                services.Configure<MvcOptions>(options =>
                {
                    // options.Filters.Add(new RequireHttpsAttribute());
                });
            }

            services.AddMvc();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<DataMarketApiClient, DataMarketApiClient>();
            services.Configure<ApiEndpoints>(Configuration.GetSection("ApiEndpoints"));

            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = 600000000;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(o =>
                {
                    o.LoginPath = new PathString("/login");
                    o.AccessDeniedPath = new PathString("/login");
                    o.Events = new CookieAuthenticationEvents()
                    {
                        OnValidatePrincipal = async (context) =>
                        {
                            bool isTokenValid = false;
                            if (!String.IsNullOrEmpty(context.HttpContext.Session.AuthenticationToken()))
                            {
                                var handler = new JwtSecurityTokenHandler();
                                SecurityToken token = handler.ReadToken(context.HttpContext.Session.AuthenticationToken());
                                isTokenValid = token.ValidTo > DateTime.UtcNow;
                            }

                            if (!isTokenValid)
                            {
                                context.RejectPrincipal();
                                context.HttpContext.Session.RemoveAuthenticationToken();
                                await Microsoft.AspNetCore.Authentication.AuthenticationHttpContextExtensions.SignOutAsync(context.HttpContext);
                            }
                        }
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            ApplicationLogging.LoggerFactory = loggerFactory;

            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error/{0}");
                app.UseStatusCodePagesWithReExecute("/Home/Error/{0}");
            }

            app.UseMvc(routes =>
             {
                 routes.MapRoute(
                     name: "default",
                     template: "{controller=Home}/{action=Index}/{id?}");
             });
        }
    }
}