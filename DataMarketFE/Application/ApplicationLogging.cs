using Microsoft.Extensions.Logging;

namespace DataMarketFE
{
    public class ApplicationLogging
    {
        private static ILoggerFactory _Factory = null;

        public static ILoggerFactory LoggerFactory
        {
            get { return _Factory; }
            set { _Factory = value; }
        }
    }
}