﻿namespace DataMarketFE
{
    public static class CustomClaimTypes
    {
        public const string Email = "Email";
        public const string CompanyId = "CompanyId";
        public const string CompanyName = "CompanyName";
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string UserId = "UserId";
    }
}