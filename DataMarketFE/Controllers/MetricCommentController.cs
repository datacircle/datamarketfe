using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    public class MetricCommentController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MetricCommentController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("metriccomment/all/{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            return ViewComponent("Comments", new CommentTypeViewModel { Type = CommentType.Metric, Id = id });
        }

        [HttpPost("metriccomment")]
        public async Task<IActionResult> Post(CommentViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(vm);
            }

            if (!await _dataMarketApiClient.PostMetricComment(vm))
            {
                return BadRequest("Error processing request.");
            }
            else
            {
                return Ok("Succesfully posted comment!");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(CommentViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(vm);
            }

            if (!await _dataMarketApiClient.DeleteMetricComment(vm.ID))
            {
                return BadRequest("Error processing request.");
            }
            else
            {
                return Ok("Succesfully deleted comment!");
            }
        }
    }
}