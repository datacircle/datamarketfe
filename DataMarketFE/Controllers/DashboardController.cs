using DataMarketFE.APIs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        public const string HistoryPanel = "History";
        public const string DashboardPanel = "Dashboard";
        public const string ProfilePanel = "Profile";
        public const string MetricsPanel = "Metric";
        public const string DatasourcesPanel = "Datasource";
        public const string RequestsPanel = "Request";
        public const string MetricTransferPanel = "MetricTransfer";

        public const string OrderPanel = "Order";

        private readonly DataMarketApiClient _dataMarketApiClient;

        public DashboardController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Panel = DashboardPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View("Index");
        }

        [HttpGet]
        [Route("history")]
        public async Task<IActionResult> History()
        {
            ViewBag.Panel = HistoryPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View(await _dataMarketApiClient.GetQueryLog());
        }
    }
}