﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ConnectorController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public ConnectorController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpPost]
        public async Task<IActionResult> CheckConnection(DatasourceViewModel datasource)
        {
            if (ModelState.IsValid && Enum.IsDefined(typeof(ViewModels.Type), datasource.Type))
            {
                return Ok(await _dataMarketApiClient.CheckAnonymConnection(datasource));
            }

            return BadRequest("Please fill in all the required fields");
        }
    }
}