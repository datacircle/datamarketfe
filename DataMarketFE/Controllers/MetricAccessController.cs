﻿using CsvHelper;
using DataMarketFE.APIs;
using DataMarketFE.Models;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    [Route("/[controller]")]
    public class MetricAccessController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MetricAccessController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }        

        [HttpPost]
        public async Task<IActionResult> Create(MetricAccessViewModel vm)
        {            
            HttpResponseMessage response = await _dataMarketApiClient.ShareMetric(vm);

            if (response.IsSuccessStatusCode)
            {
                MetricAccessMapModel _vm =
                    JsonConvert.DeserializeObject<MetricAccessMapModel>(await response.Content.ReadAsStringAsync());
                return Ok(_vm);
            }

            return BadRequest(await response.Content.ReadAsStringAsync());
        }

        [HttpDelete("{shareMetricId}")]
        public async Task<IActionResult> Delete(int shareMetricId)
        {
            HttpResponseMessage response = await _dataMarketApiClient.DeleteShareMetric(shareMetricId);

            if (response.IsSuccessStatusCode)
            {
                return Ok();
            }

            return BadRequest(response.RequestMessage);
        }
    }
}