﻿using Microsoft.AspNetCore.Mvc;

namespace DataMarketFE.Controllers
{
    public class HomeController : Controller
    {
        [Route("about")]
        public IActionResult Index()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("privacy")]
        public IActionResult Privacy()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View();
        }

        [Route("tos")]
        public IActionResult TermsOfService()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View();
        }

        [Route("imprint")]
        public IActionResult Imprint()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View();
        }

        [Route("contact")]
        public IActionResult Contact()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View();
        }

        // [Route("pricing")]
        // public IActionResult Pricing()
        // {
        //     ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
        //     return View();
        // }

        [Route("system")]
        public IActionResult System()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("careers")]
        public IActionResult Careers()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("blog")]
        public IActionResult Blog()
        {
            return RedirectPermanent("https://blog.datacircle.io");
        }

        [Route("features")]
        public IActionResult Features()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("solutions/consumers")]
        public IActionResult ConsumerSolution()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View("Solutions/Consumers");
        }

        [Route("solutions/providers")]
        public IActionResult ProviderSolution()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View("Solutions/Providers");
        }

        [Route("solutions/collaboration")]
        public IActionResult CollaborationSolution()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View("Solutions/Collaboration");
        }

        [Route("solutions/opensource")]
        public IActionResult OpensourceSolution()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View("Solutions/Opensource");
        }

        [Route("newsletter")]
        public IActionResult Newsletter()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("survey")]
        public IActionResult Survey()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("factory")]
        public IActionResult Factory()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("ahoy")]
        public IActionResult Ahoy()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("datanatives")]
        public IActionResult Datanatives()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("producthunt")]
        public IActionResult Producthunt()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("media")]
        public IActionResult Media()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return View();
        }

        [Route("/Home/Error/{ErrorCode}")]
        public IActionResult Error(int ErrorCode)
        {
            if (ErrorCode >= 400 && ErrorCode < 500)
            {
                ViewData["Title"] = "Oops!";
                return View("Error/404");
            }
            else
            {
                ViewData["Title"] = "Technician has been informed";
                return View("Error/500");
            }
        }

        [Route("/404", Name = "404")]
        [HttpGet]
        public IActionResult Page404()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            ViewBag.Panel = "";
            ViewData["Title"] = "Oops!";
            return View("Error/404");
        }

        [Route("/500")]
        [HttpGet]
        public IActionResult Page500()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            ViewBag.Panel = "";
            ViewData["Title"] = "Technician has been informed";
            return View("Error/500");
        }

        [Route("/503")]
        [HttpGet]
        public IActionResult Page503()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            ViewBag.Panel = "";
            ViewData["Title"] = "Service under Maintenance.";
            return View("Error/503");
        }
    }
}