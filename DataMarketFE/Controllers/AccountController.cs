using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Route("[action]")]
    public class AccountController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public AccountController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        //
        // GET: /Profile
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Profile(string returnUrl = null)
        {
            ViewBag.Panel = DashboardController.ProfilePanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View(await _dataMarketApiClient.GetUserProfile());
        }

        //
        // POST: /UpdateProfile
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateProfile(ProfileViewModel vm, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View("Profile", vm);
            }
            else
            {
                await _dataMarketApiClient.UpdateProfile(vm);
                TempData["flash-success-msg"] = "Your Profile information has been updated!";
                return Redirect("/profile");
            }
        }

        //
        // GET: /Login
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            if (returnUrl != null)
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(AccountController)).LogInformation("Incoming returnUrl: " + returnUrl);
            }

            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            if (ViewBag.LoggedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //
        // POST: /Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (HttpContext.IsUserLoggedIn())
            {
                return RedirectToLocal(returnUrl);
            }

            if (ModelState.IsValid)
            {
                HttpResponseMessage result = await _dataMarketApiClient.Login(model);

                if (!result.IsSuccessStatusCode)
                {
                    if (result.StatusCode.ToString() == "422")
                    {
                        ModelState.AddModelError(string.Empty, "A combination of those credentials did not match.");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error logging in. Please try again.");
                    }
                    return View();
                }

                HttpContext.Session.SetAuthenticationToken(await result.Content.ReadAsStringAsync());
                ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

                if (result.IsSuccessStatusCode)
                {
                    await LogInAnonymous(model.Email);
                    return RedirectToLocal(returnUrl ?? "/dashboard");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, await result.Content.ReadAsStringAsync());
                }
            }

            return View(model);
        }

        private async Task<bool> LogInAnonymous(string Email)
        {
            const string Issuer = "https://www.datacircle.io";
            ClaimsIdentity userIdentity = new ClaimsIdentity("SecuredLoggedIn");
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, Email, ClaimValueTypes.String, Issuer));
            userIdentity.AddClaims(claims);
            await Microsoft.AspNetCore.Authentication.AuthenticationHttpContextExtensions.SignInAsync(HttpContext, new ClaimsPrincipal(userIdentity));

            // var userPrincipal = new ClaimsPrincipal(userIdentity);

            //         // Sign in the user creating a cookie with X ammount of Expiry
            //         await HttpContext.Authentication.SignInAsync("Cookie", userPrincipal,
            //             new AuthenticationProperties
            //             {
            //                 ExpiresUtc = DateTime.UtcNow.AddHours(1),
            //                 IsPersistent = false,
            //                 AllowRefresh = false
            //             });

            return true;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            if (ViewBag.LoggedIn)
            {
                return RedirectToAction("Index", "Dashboard");
            }


            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (HttpContext.IsUserLoggedIn())
            {
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                HttpResponseMessage result = await _dataMarketApiClient.Register(model);

                if (result.IsSuccessStatusCode)
                {
                    TempData["flash-success-msg"] = "You have been registed! You can now login.";
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    TempData["flash-danger-msg"] = await result.Content.ReadAsStringAsync();
                    ModelState.AddModelError(string.Empty, await result.Content.ReadAsStringAsync());
                }
            }

            return View(model);
        }

        //
        // GET: /RequestBeta
        [HttpGet]
        [AllowAnonymous]
        public IActionResult RequestBeta(string returnUrl = null)
        {
            return LocalRedirectPermanent("/register");
        }

        //
        // Load view
        // GET: /ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = "/";
            ViewBag.LoggedIn = false;
            ViewBag.Panel = "ForgotPasswordPanel";
            return View();
        }

        //
        // Load view
        // GET: /ResetPassword/{id}
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword([FromQuery] string token)
        {
            if (token != null && token.Length > 0)
            {
                ViewData["ReturnUrl"] = "/";
                ViewBag.LoggedIn = false;
                return View();
            }
            else
            {
                return RedirectToLocal("/");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel vm)
        {
            // string referer = HttpContext.Request.Headers["Referer"].ToString();

            // var start = referer.LastIndexOf("/") == -1 ? 0 : referer.LastIndexOf("/") + 1;
            // var end = referer.LastIndexOf("?returnurl=%2F") == -1 ? 0 : referer.LastIndexOf("?returnurl=%2F");
            // var resta = end == 0 ? 0 : ("?returnurl=%2F").Length;
            // var length = referer.Length - start - resta;
            // string token = referer.Substring(start, length);

            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            else
            {
                HttpResponseMessage result = await _dataMarketApiClient.ResetPassword(vm);

                if (result.IsSuccessStatusCode)
                {
                    TempData["flash-success-msg"] = "Your password has been changed. You can now login.";
                    ViewBag.LoggedIn = false;
                    return Redirect("/login");
                }
            }

            TempData["flash-danger-msg"] = "Something went wrong with your form please try again.";
            ViewData["ReturnUrl"] = "/";
            ViewBag.LoggedIn = false;
            return RedirectToLocal(HttpContext.Request.Headers["Referer"].ToString());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RequestResetPassword(string email)
        {
            HttpResponseMessage result = await _dataMarketApiClient.RequestPasswordReset(email);

            if (result.IsSuccessStatusCode)
            {
                TempData["flash-success-msg"] = "Your password has been reset.";
            }
            else
            {
                TempData["flash-danger-msg"] = "There was an error processing your request.";
                return Redirect("ForgotPassword");
            }

            return Redirect("/");
        }

        //
        // Get: /LogOff
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.RemoveAuthenticationToken();
            await Microsoft.AspNetCore.Authentication.AuthenticationHttpContextExtensions.SignOutAsync(HttpContext);

            ViewBag.LoggedIn = false;
            return Redirect("/");
        }

        private IActionResult RedirectToLocal(string returnUrl = null)
        {
            ApplicationLogging.LoggerFactory.CreateLogger(nameof(AccountController)).LogInformation("Enter Redirect To Local: " + returnUrl + " RESULT[" + Url.IsLocalUrl(returnUrl).ToString() + "]");
            if (returnUrl == null)
            {
                return RedirectToAction(nameof(DashboardController.Index), "Dashboard");
            }

            if (Url.IsLocalUrl(returnUrl))
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(AccountController)).LogInformation("Actual Redirect To Local: " + returnUrl);
                return Redirect(returnUrl);
            }
            else
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(AccountController)).LogInformation("Denied Redirect To Local: " + returnUrl);
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}