using System.Threading.Tasks;
using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketFE.Controllers
{
    [Route("/[controller]")]
    public class BusinessController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public BusinessController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("")]
        public IActionResult Index()
        {
            ViewBag.Market = true;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View();
        }     

        [HttpPost]
        public async Task<IActionResult> Create(BusinessInquery model)
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }
            else
            {
                await _dataMarketApiClient.PostAsync("https://hooks.zapier.com/hooks/catch/2808065/8btp41/", model);
                TempData["flash-success-msg"] = "We received your request and will contact you shortly.";
                return Redirect("/");
            }
        }       
    }
}