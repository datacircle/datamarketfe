﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    public class SearchController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public SearchController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("/search/view/")]
        public async Task<IActionResult> SearchView([FromQuery] string searchTerm, [FromQuery] string hashTagTerm)
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            HttpResponseMessage _response = await _dataMarketApiClient.SearchMetric(searchTerm, hashTagTerm);

            if (!_response.IsSuccessStatusCode)
            {
                return PartialView(new List<MarketplaceMetricViewModel>());
            }

            IEnumerable<MarketplaceMetricViewModel> _metricVm =
                JsonConvert.DeserializeObject<IEnumerable<MarketplaceMetricViewModel>>(await _response.Content.ReadAsStringAsync());

            return PartialView(_metricVm);
        }

        [HttpGet("/search")]
        public async Task<IActionResult> Search([FromQuery] string searchTerm, [FromQuery] string hashTagTerm)
        {
            HttpResponseMessage _response = await _dataMarketApiClient.SearchMetric(searchTerm, hashTagTerm);

            if (!_response.IsSuccessStatusCode)
            {
                return BadRequest();
            }

            IEnumerable<MarketplaceMetricViewModel> _metricVm =
                JsonConvert.DeserializeObject<IEnumerable<MarketplaceMetricViewModel>>(await _response.Content.ReadAsStringAsync());

            return Ok(_metricVm);
        }

        [HttpGet("/search/hashtag/{searchTerm}")]
        public async Task<IActionResult> HashTagSearch(string searchTerm)
        {
            if (searchTerm.Length < 2)
            {
                return Ok();
            }

            HttpResponseMessage _response = await _dataMarketApiClient.SearchHashTag(searchTerm);

            if (!_response.IsSuccessStatusCode)
            {
                return BadRequest();
            }

            IEnumerable<HashTagViewModel> _hashTag =
                JsonConvert.DeserializeObject<IEnumerable<HashTagViewModel>>(await _response.Content.ReadAsStringAsync());

            return Ok(_hashTag);
        }
    }
}