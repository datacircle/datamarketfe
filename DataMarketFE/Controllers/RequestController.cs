using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    public class RequestController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public RequestController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("requests")]
        public async Task<IActionResult> Requests()
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            IEnumerable<RequestViewModel> requestCollection = await _dataMarketApiClient.GetRequestAll(true);

            if (requestCollection == null)
            {
                requestCollection = new List<RequestViewModel>();
            }

            return View(requestCollection);
        }

        [HttpGet("requests/{title}/{id}/", Name = "GetRequest")]
        public async Task<IActionResult> GetRequest([FromRoute] Guid id, string title)
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            RequestViewModel request = await _dataMarketApiClient.GetRequest(id, true);       

            if (request == null)
            {
                return NotFound();
            } 

            string friendlyTitle = Utilities.FriendlyUrlHelper.GetFriendlyTitle(request.Title);

            if (!string.Equals(friendlyTitle, title, StringComparison.Ordinal))
            {
                return this.RedirectToRoutePermanent("GetRequest", new { id = id, title = friendlyTitle });
            }

            return View("Request", request);
        }

        [HttpGet("requests/upvote/{id}")]
        public async Task<IActionResult> Upvote(Guid id)
        {
            if (await _dataMarketApiClient.Upvote(id))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet("requests/downvote/{id}")]
        public async Task<IActionResult> Downvote(Guid id)
        {
            if (await _dataMarketApiClient.Downvote(id))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet("request/{id?}")]
        public async Task<IActionResult> Index([FromRoute] Guid id)
        {
            ViewBag.Panel = DashboardController.RequestsPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            RequestViewModel request = new RequestViewModel();

            if (id != Guid.Empty)
            {
                RequestViewModel result = await _dataMarketApiClient.GetRequest(id);
                if (result != null)
                {
                    request = result;
                }
            }

            return View(request);
        }

        [Authorize]
        [HttpPost("request/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([FromRoute] Guid id, RequestViewModel vm)
        {
            ViewBag.Panel = DashboardController.RequestsPanel;

            if (!ModelState.IsValid)
            {
                return View(vm);
            }        

            bool result;
            if (vm.ID == Guid.Empty)
            {
                vm = await _dataMarketApiClient.CreateRequest(vm);
                result = (vm != null);
            }
            else
            {
                result = await _dataMarketApiClient.UpdateRequest(vm);
            }

            if (!result)
            {
                TempData["flash-danger-msg"] = "Error processing request.";
                return View("Index", vm);
            }
            else
            {
                TempData["flash-success-msg"] = "Succesfully submitted request!";
            }

            string idValue = string.Empty;
            if (vm.ID != Guid.Empty)
            {
                idValue = vm.ID.ToString();
            }

            return RedirectToAction(nameof(RequestController.Index), new { id = idValue });
        }

        [Authorize]
        [HttpPost("DeleteRequest")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(RequestViewModel vm)
        {
            ViewBag.Panel = DashboardController.RequestsPanel;
            bool result = await _dataMarketApiClient.DeleteRequest(vm.ID);

            if (!result)
            {
                TempData["flash-danger-msg"] = "Error processing request.";
                return RedirectToAction(nameof(RequestController.Index));
            }

            TempData["flash-success-msg"] = "Succesfully deleted request!";
            return RedirectToAction(nameof(RequestController.Index));
        }
    }
}