using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    public class DatasourceController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public DatasourceController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("datasource/{id?}")]
        public async Task<IActionResult> Index([FromRoute] Guid id)
        {
            ViewBag.Panel = DashboardController.DatasourcesPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            DatasourceViewModel datasource = new DatasourceViewModel();

            if (id != Guid.Empty)
            {
                DatasourceViewModel result = await _dataMarketApiClient.GetDatasource(id);
            
                if (result != null)
                {
                    datasource = result;
                }
            }

            return View(datasource);
        }

        [HttpPost("datasource/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([FromRoute] Guid id, DatasourceViewModel vm)
        {
            ViewBag.Panel = DashboardController.DatasourcesPanel;

            if (vm.Type == 0)
            {
                ModelState.AddModelError("Type", "Please select a Type.");
            }

            if (vm.Port == 0)
            {
                ModelState.AddModelError("Port", "Please select a Port number.");
            }

            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            bool result = false;
            if (vm.ID == Guid.Empty)
            {
                vm = await _dataMarketApiClient.CreateDatasource(vm);
                if (vm != null)
                {
                    result = true;
                }
            }
            else
            {
                result = await _dataMarketApiClient.UpdateDatasource(vm);
            }

            if (!result)
            {
                TempData["flash-danger-msg"] = "Error processing request.";
                return View("Index", vm);
            }
            else
            {
                TempData["flash-success-msg"] = "Succesfully submitted datasource!";
            }

            string idValue = string.Empty;
            if (vm.ID != Guid.Empty)
            {
                idValue = vm.ID.ToString();
            }

            return RedirectToAction(nameof(DatasourceController.Index), new { id = idValue });
        }

        [HttpPost("DeleteDatasource")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(DatasourceViewModel vm)
        {
            ViewBag.Panel = DashboardController.DatasourcesPanel;
            HttpResponseMessage result = await _dataMarketApiClient.DeleteDatasource(vm.ID);

            if (!result.IsSuccessStatusCode)
            {
                TempData["flash-danger-msg"] = await result.Content.ReadAsStringAsync();
                return RedirectToAction(nameof(DatasourceController.Index), new { id = vm.ID });
            }

            TempData["flash-success-msg"] = "Succesfully deleted datasource!";
            return RedirectToAction(nameof(DatasourceController.Index));
        }
    }
}