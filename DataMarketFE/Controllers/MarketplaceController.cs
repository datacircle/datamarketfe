using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    public class MarketplaceController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MarketplaceController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("/")]
        public async Task<IActionResult> Index([FromQuery] string searchTerm, [FromQuery] string hashTagTerm)
        {
            ViewBag.Market = true;

            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            MarketplaceViewModel vm = new MarketplaceViewModel();
            vm.SearchTerm = searchTerm;
            vm.HashTagTerm = hashTagTerm;

            if (
                (searchTerm == null || searchTerm == String.Empty) &&
                (hashTagTerm == null || hashTagTerm == String.Empty)
                )
            {
                return View(vm);
            }

            if ((searchTerm != null && searchTerm.Length < 2) || (hashTagTerm != null && hashTagTerm.Length < 2))
            {
                TempData["flash-danger-msg"] = "Please enter a search term longer tha 2 characters";
                return View(vm);
            }

                List<MetricViewModel> _metricCollection = new List<MetricViewModel>();

                HttpResponseMessage response = await _dataMarketApiClient.SearchMetric(searchTerm, hashTagTerm);

                if (response.IsSuccessStatusCode)
                {
                    vm.Metrics = JsonConvert.DeserializeObject<IEnumerable<MarketplaceMetricViewModel>>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    TempData["flash-danger-msg"] = "Error processing search term.";
                }
            

            return View(vm);
        }

        [HttpGet("marketplace/{searchTerm?}")]
        public IActionResult Marketplace([FromQuery] string searchTerm)
        {
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();
            return LocalRedirectPermanentPreserveMethod("~/?searchTerm=" + searchTerm); 
        }
    }
}