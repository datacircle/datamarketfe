using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class RatingController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public RatingController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpPost]
        public async Task<IActionResult> Index(Guid id, RatingViewModel vm)
        {
            ViewBag.Panel = DashboardController.RequestsPanel;

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            string result;
            if (vm.ID == 0)
            {
                result = await _dataMarketApiClient.CreateRating(vm);
            }
            else
            {
                result = await _dataMarketApiClient.UpdateRating(vm);
            }

            if (result == string.Empty)
            {
                return BadRequest();
            }
            else
            {
                return Ok();
            }
        }

        [HttpPost("DeleteRating")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(RatingViewModel vm)
        {
            string result = await _dataMarketApiClient.DeleteRating(vm.ID);

            if (result == string.Empty)
            {
                return BadRequest("Error deleting rating");
            }

            return Ok();
        }
    }
}