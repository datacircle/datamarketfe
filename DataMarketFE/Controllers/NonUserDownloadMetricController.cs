﻿using CsvHelper;
using DataMarketFE.APIs;
using DataMarketFE.Models;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Route("/[controller]/[action]")]
    public class NonUserDownloadMetricController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public NonUserDownloadMetricController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        
        [HttpPost]
        public async Task<IActionResult> Index([FromForm] FreeMetricDownloadViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please fill in all the required fields");
            }
            
            HttpResponseMessage result = await _dataMarketApiClient.RequestFreeMetricDownload(vm);

            try
            {
                if (result.IsSuccessStatusCode)
                {
                    return Ok();
                }

                var response = await result.Content.ReadAsStringAsync();
                return BadRequest(response);
            }
            catch (Exception e)
            {
                return BadRequest("There has been an error. Please contact support.");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddEmail([FromForm] FreeMetricDownloadViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please fill in all the required fields");
            }
            
            HttpResponseMessage result = await _dataMarketApiClient.JustAddEmail(vm);

            try
            {
                if (result.IsSuccessStatusCode)
                {
                    return Ok();
                }

                var response = await result.Content.ReadAsStringAsync();
                return BadRequest(response);
            }
            catch (Exception e)
            {
                return BadRequest("There has been an error. Please contact support.");
            }
        }

        [HttpGet("{metricId}")]
        public async Task<IActionResult> Download([FromRoute] Guid metricId)
        {
            if (metricId == Guid.Empty)
            {
                return BadRequest();
            }            

            HttpResponseMessage responseMessage = await _dataMarketApiClient.DownloadFreeMetric(metricId);

            if (responseMessage.IsSuccessStatusCode)
            {
                DataStoreCollectionModel _dsc = JsonConvert.DeserializeObject<DataStoreCollectionModel>(await responseMessage.Content.ReadAsStringAsync());

                if (!string.IsNullOrEmpty(_dsc.FileFullPath))
                {
                    var stream = new FileStream(_dsc.FileFullPath, FileMode.Open, FileAccess.Read);
                    return new FileStreamResult(stream, "application/octet-stream")
                    {
                        FileDownloadName = Path.GetFileName(_dsc.FileFullPath) + "" + Path.GetExtension(_dsc.FileFullPath)
                    };
                }
                else
                {
                    var result = WriteCsvToMemory(_dsc);
                    var memoryStream = new MemoryStream(result);

                    return new FileStreamResult(memoryStream, "text/csv") { FileDownloadName = $"{_dsc.FileName}.csv" };
                }
            }

            return BadRequest(await responseMessage.Content.ReadAsStringAsync());
        }

        public byte[] WriteCsvToMemory(DataStoreCollectionModel dsc)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                foreach (string col in dsc.Structure.Keys)
                {
                    csvWriter.WriteField(col);
                }
                csvWriter.NextRecord();

                for (int i = 0; i < dsc.Data.Count; i++)
                {
                    foreach (var col in dsc.Data[i])
                    {
                        csvWriter.WriteField(col);
                    }

                    csvWriter.NextRecord();
                }

                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }
    }
}