using DataMarketFE.APIs;
using DataMarketFE.Extensions;
using DataMarketFE.Filters;
using DataMarketFE.Models;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    public class MetricController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        private static readonly FormOptions _defaultFormOptions = new FormOptions();

        public MetricController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [Authorize]
        [HttpGet("metric")]
        public IActionResult Index([FromQuery] Guid datasourceId)
        {
            if (datasourceId != Guid.Empty)
            {
                ViewBag.AutoSelectDatasourceId = datasourceId;
            }

            ViewBag.Panel = DashboardController.MetricsPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View(new MetricViewModel());
        }

        [HttpGet("metric/{id}/")]
        public async Task<IActionResult> GetMetricNoTitle([FromRoute] Guid id, string title = "")        
        {
            return await Metric(id, title);
        }

        [HttpGet("metric/{title}/{id}/", Name = "GetMetric")]
        public async Task<IActionResult> GetMetric([FromRoute] Guid id, string title = "")
        {
            return await Metric(id, title);
        }

        private async Task<IActionResult> Metric([FromRoute] Guid id, string title = "")
        {
            ViewBag.Panel = DashboardController.MetricsPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            MetricViewModel metric = await _dataMarketApiClient.GetMetric(id);

            if (metric == null)
            {
                return this.Render404();
            }

            string friendlyTitle = Utilities.FriendlyUrlHelper.GetFriendlyTitle(metric.Title);

            if (HttpContext.GetUserId() != metric.Owner || ViewBag.LoggedIn == false)
            {
                if (!string.Equals(friendlyTitle, title, StringComparison.Ordinal))
                {
                    return this.RedirectToRoutePermanent("GetMetric", new { id = id, title = friendlyTitle });
                }

                OrderViewModel order = await _dataMarketApiClient.GetOrderStatus(metric.ID);

                MetricOverviewViewModel movm = new MetricOverviewViewModel();
                movm.Hydrate(metric);
                movm.Order = order;

                return View("3rdPartyView", movm);
            }

            return View("Index", metric);
        }

        [Authorize]
        [HttpPost("metric/{id?}")]
        [DisableFormValueModelBinding]
        [RequestSizeLimit(4400000000)]
        //[DisableRequestSizeLimit]
        public async Task<IActionResult> Upsert()
        {
            string displayUrl = HttpContext.Request.GetDisplayUrl();
            var arrayUrl = displayUrl.Split('/');
            string metricId = arrayUrl.ToList().Last();

            if (!this.IsMultipartContentType(Request.ContentType))
            {
                return BadRequest($"Expected a multipart request, but got {Request.ContentType}");
            }

            FileShareModel file = new FileShareModel();

            var formAccumulator = new KeyValueAccumulator();

            var boundary = this.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary.ToString(), HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();

            while (section != null)
            {
                var fileSection = section.AsFileSection();
                if (fileSection != null)
                {
                    file.FileName = fileSection.FileName;
                    file.FilePath = Path.GetTempFileName();

                    using (Stream streamLocal = new FileStream(file.FilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        if (section.Body.Length < 65536)
                        {
                            await section.Body.CopyToAsync(streamLocal);
                        }
                        else
                        {
                            byte[] buffer = new byte[65536];
                            int bytesRead;

                            while ((bytesRead = HttpContext.Request.Body.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                streamLocal.Write(buffer, 0, bytesRead);
                            }
                        }
                    }
                }
                else
                {
                    var formSection = section.AsFormDataSection();
                    if (formSection != null)
                    {
                        var name = formSection.Name;
                        var value = await formSection.GetValueAsync();

                        formAccumulator.Append(name, value);

                        if (formAccumulator.ValueCount > FormReader.DefaultValueCountLimit)
                        {
                            throw new InvalidDataException(
                                $"Form key count limit {FormReader.DefaultValueCountLimit} exceeded.");
                        }
                    }
                }

                section = await reader.ReadNextSectionAsync();
            }

            MetricViewModel metric = await BindModel(formAccumulator.GetResults());

            if (metric != null && !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (file.FileName != null && file.FilePath != null)
            {
                metric.FileName = file.FileName;
                metric.FilePath = file.FilePath;
            }

            bool result = false;
            MetricViewModel resultVm;

            if (metric.ID == Guid.Empty)
            {
                resultVm = await _dataMarketApiClient.CreateMetric(metric);
                if (resultVm != null)
                {
                    result = true;
                    metric = resultVm;
                }
            }
            else
            {
                result = await _dataMarketApiClient.UpdateMetric(metric);
            }

            if (!result)
            {
                return BadRequest("Error processing request.");
            }
            else
            {
                TempData["flash-success-msg"] = "Succesfully submitted data set!";
                file.MetricId = metric.ID;
                return Ok(metric.ID);
            }
        }

        private async Task<MetricViewModel> BindModel(Dictionary<string, Microsoft.Extensions.Primitives.StringValues> formResults)
        {
            var metric = new MetricViewModel();
            var formValueProvider = new FormValueProvider(
                            BindingSource.Form,
                            new FormCollection(formResults),
                            CultureInfo.CurrentCulture);

            await TryUpdateModelAsync(metric, prefix: "", valueProvider: formValueProvider);

            return metric;
        }

        [Authorize]
        [HttpPost("DeleteMetric")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(MetricViewModel vm)
        {
            ViewBag.Panel = DashboardController.MetricsPanel;
            bool result = await _dataMarketApiClient.DeleteMetric(vm.ID);

            if (!result)
            {
                TempData["flash-danger-msg"] = "Error processing request.";
                return RedirectToAction(nameof(MetricController.Index), new { id = vm.ID });
            }

            TempData["flash-success-msg"] = "Succesfully deleted metric!";
            return RedirectToAction(nameof(MetricController.Index));
        }

        [Authorize]
        [HttpGet("/trigger/{id}")]
        public async Task<IActionResult> Trigger(Guid id)
        {
            switch (await _dataMarketApiClient.TriggerDataPool(id))
            {
                case System.Net.HttpStatusCode.OK:
                    return Ok();

                case System.Net.HttpStatusCode.NoContent:
                    return Ok("No Data");

                default:
                    return BadRequest();
            }
        }
    }
}