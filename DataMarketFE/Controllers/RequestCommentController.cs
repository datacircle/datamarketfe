using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    public class RequestCommentController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public RequestCommentController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("requestcomment/all/{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            return ViewComponent("Comments", new CommentTypeViewModel { Type = CommentType.Request, Id = id } );
        }

        [HttpPost("requestcomment")]
        public async Task<IActionResult> Post(CommentViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(vm);
            }

            if (!await _dataMarketApiClient.PostRequestComment(vm))
            {
                return BadRequest("Error processing request.");
            }
            else
            {
                return Ok("Succesfully posted comment!");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(CommentViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(vm);
            }

            if (!await _dataMarketApiClient.DeleteRequestComment(vm.ID))
            {
                return BadRequest("Error processing request.");
            }
            else
            {
                return Ok("Succesfully deleted comment!");
            }
        }
    }
}