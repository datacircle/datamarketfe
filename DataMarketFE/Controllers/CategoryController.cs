﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DataMarketFE.Controllers
{
    [Route("/[controller]")]
    public class CategoryController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public CategoryController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet("{categoryName}")]
        public async System.Threading.Tasks.Task<IActionResult> Index(string categoryName)
        {
            CategoryViewModel _c = await _dataMarketApiClient.GetMetricCategory(categoryName);

            if (_c == null)
            {
                return NotFound();
            }

            ViewData["Title"] = _c.Name;
            return View(_c);
        }        
    }
}