﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.Controllers
{
    [Authorize]
    [Route("/[controller]")]
    public class OrderController : Controller
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public OrderController(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.Panel = DashboardController.OrderPanel;
            ViewBag.LoggedIn = HttpContext.IsUserLoggedIn();

            return View(await _dataMarketApiClient.GetAllOrders());
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] OrderViewModel vm)
        {
            HttpResponseMessage response = await _dataMarketApiClient.Order(vm);

            if (response.IsSuccessStatusCode)
            {
                return Ok();
            }

            return BadRequest(await response.Content.ReadAsStringAsync());
        }
    }
}