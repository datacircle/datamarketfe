using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace DataMarketFE
{
    public static class HttpContextExtensions
    {
        public static bool IsUserLoggedIn(this HttpContext httpContext)
        {
            return httpContext.Session.AuthenticationToken() != null &&
                !(httpContext.Session.AuthenticationToken().Length == 0);
        }

        public static string GetIdentityCookie(this HttpContext httpContext)
        {
            var request = httpContext.Request;
            return httpContext.Session.GetString("IdentityCookieId");
        }

        public static int GetUserId(this HttpContext httpContext)
        {
            if (string.IsNullOrEmpty(httpContext.Session.AuthenticationToken()))
            {
                return -1;
            }

            JwtSecurityToken token = new JwtSecurityToken(httpContext.Session.AuthenticationToken());

            string userId = token.Claims.First(claim => claim.Type == CustomClaimTypes.UserId).Value;

            if (!Int32.TryParse(userId, out int x))
            {
                throw new Exception("Could not parse " + CustomClaimTypes.UserId);
            }

            return x;
        }
    }
}