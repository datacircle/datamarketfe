using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace DataMarketFE
{
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PostAsJsonAsync(this HttpClient client, string addr, object obj, string token = null)
        {
            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);           

            HttpContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

            try
            {
                return await client.PostAsync(addr, content);
            }
            catch (Exception e)
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(HttpClientExtensions)).LogError("Error accessing Path " + addr + Environment.NewLine + e.ToString());
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }
        }

        public static async Task<HttpResponseMessage> Get(this HttpClient client, string addr, string token = null)
        {
            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            try
            {
                return await client.GetAsync(addr);
            }
            catch (Exception e)
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(HttpClientExtensions)).LogError("Error accessing Path " + addr + Environment.NewLine + e.ToString());
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }
        }

        public static async Task<HttpResponseMessage> Put(this HttpClient client, string addr, object obj, string token = null)
        {                         
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

            try
            {
                return await client.PutAsync(addr, content);
            }
            catch (Exception e)
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(HttpClientExtensions)).LogError("Error accessing Path " + addr + Environment.NewLine + e.ToString());
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }
        }

        public static async Task<HttpResponseMessage> Delete(this HttpClient client, string addr, string token = null)
        {
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            try
            {
                return await client.DeleteAsync(addr);
            }
            catch (Exception e)
            {
                ApplicationLogging.LoggerFactory.CreateLogger(nameof(HttpClientExtensions)).LogError("Error accessing Path " + addr + Environment.NewLine + e.ToString());
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }
        }

        public static string ContentToString(this HttpContent httpContent)
        {
            return httpContent.ReadAsStringAsync().Result;
        }
    }
}