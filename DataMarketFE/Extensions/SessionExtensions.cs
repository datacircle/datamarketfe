using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DataMarketFE
{
    public static class SessionExtensions
    {
        private const string AuthTokenKey = "authToken";

        /// <summary>
        /// Store a comples object in session
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetObject(this ISession session, string key, object value)
        {
            session.Set(key, ToByteArray(JsonConvert.SerializeObject(value)));
        }

        /// <summary>
        /// Gets a complex object in session of the specified type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetObject<T>(this ISession session, string key)
        {
            T value = default(T);
            byte[] bytes;
            if (session.TryGetValue(key, out bytes))
            {
                value = JsonConvert.DeserializeObject<T>(GetString(bytes));
            }
            return value;
        }

        private static byte[] ToByteArray(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static string AuthenticationToken(this ISession session)
        {
            return session.GetString(AuthTokenKey);
        }

        public static void SetAuthenticationToken(this ISession session, string token)
        {
            session.SetString(AuthTokenKey, token);
        }

        public static void RemoveAuthenticationToken(this ISession session)
        {
            session.Remove(AuthTokenKey);
        }
    }
}