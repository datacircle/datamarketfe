﻿namespace DataMarketFE.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int UsageCount { get; set; }
    }
}