﻿using System;

namespace DataMarketFE.ViewModels
{
    public enum CommentType
    {
        Request = 1,
        Metric = 2
    }

    public class CommentTypeViewModel
    {
        public CommentType Type { get; set; }

        public Guid Id { get; set; }
    }
}