﻿using System;

namespace DataMarketFE.ViewModels
{
    public enum OrderStatus
    {
        New = 0,

        Paid = 1,

        Returned = -1,
    }

    public class OrderViewModel
    {
        public string PublicId { get; set; }

        public int Owner { get; set; }

        public ProfileViewModel User { get; set; }

        public int Cost { get; set; }

        public Guid? MetricId { get; set; }

        public bool Test { get; set; } = false;

        public OrderStatus Status { get; set; } = OrderStatus.New;

        public int? InvoiceId { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}

