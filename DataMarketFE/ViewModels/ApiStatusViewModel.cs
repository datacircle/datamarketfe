namespace DataMarketFE.ViewModels
{
    public class ApiStatusViewModel
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}