﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class MetricAccessViewModel
    {
        [Required]
        public Guid MetricId { get; set; }

        public string Email { get; set; }

        public string TargetPublicId { get; set; }
    }
}
