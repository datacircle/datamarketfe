﻿using System;

namespace DataMarketFE.ViewModels
{
    public class HashTagViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int UsageCount { get; set; }

        public Guid MetricId { get; set; }

        public MetricViewModel Metric { get; set; }
    }
}