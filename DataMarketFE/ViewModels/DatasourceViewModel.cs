using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public enum Type
    {
        FileDownload = 0,

        FileUploadGeneric = 1,

        Link = 2,

        Airtable = 3,

        Mysql = 100,

        Mssql = 101
    }

    public class DatasourceViewModel
    {
        public Guid ID { get; set; }

        public int? CompanyId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at max {1} characters long.")]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at max {1} characters long.")]
        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        public Type Type { get; set; }

        [Required]
        public string Host { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        public string Database { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public int Port { get; set; }
    }
}