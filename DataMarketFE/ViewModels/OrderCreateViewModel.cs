﻿using System;

namespace DataMarketFE.ViewModels
{
    public class OrderCreateViewModel
    {
        public string FullName { get; set; }
        public Guid MetricId { get; set; }
        public string Address { get; set; }
    }   
}

