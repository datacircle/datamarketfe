﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class RatingViewModel
    {
        public int ID { get; set; }

        [Required]
        public double Value { get; set; }

        [Required]
        public Guid MetricID { get; set; }

        public MetricViewModel Metric { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}