using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class QueryLogViewModel
    {
        [Required]
        public Guid DatasourceID { get; set; }

        [Required]
        public DatasourceViewModel Datasource { get; set; }

        [Required]
        public Guid MetricID { get; set; }

        [Required]
        public MetricViewModel Metric { get; set; }

        [Required]
        public bool Cached { get; set; } = false;

        public long QueryTime { get; set; } = 0;

        public int DataSize { get; set; } = 0;

        public DateTime CreatedAt { get; set; }
    }
}