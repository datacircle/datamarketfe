﻿namespace DataMarketFE.ViewModels
{
    public class StructureItemViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}