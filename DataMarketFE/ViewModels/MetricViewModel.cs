using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class MetricViewModel
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(72, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Title { get; set; }

        [StringLength(10000, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Description { get; set; }

        public string DescriptionPlainText { get; set; }

        public double Rating { get; set; }

        public int CommentCount { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select a Category.")]
        public int? CategoryId { get; set; }

        [Required]
        [StringLength(400, MinimumLength = 2, ErrorMessage = "Please enter at least one tag.")]
        public string HashTags { get; set; }

        #region Privacy

        public bool Approved { get; set; } = false;
        public string Licence { get; set; }
        public bool Open { get; set; } = false;
        public bool ShareLinkEnabled { get; set; } = false;
        public bool RequiresAuthorization { get; set; } = false;

        #endregion Privacy

        #region Process

        public int Interval { get; set; } = 1800;
        public DateTime LastPool { get; set; }
        public bool Processing { get; set; } = false;

        #endregion Process


        public string Structure { get; set; }

        #region DB

        public string Query { get; set; }

        #endregion DB

        #region Link

        public string Url { get; set; }

        #endregion Link

        #region File

        public string FileName { get; set; }

        public string FilePath { get; set; }

        #endregion File

        [Required]
        public Guid DatasourceId { get; set; }

        public Type DatasourceType { get; set; }

        public DatasourceViewModel Datasource { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Price should not contain decimals.")]
        public int Cost { get; set; } = 0;

        public bool Anonymous { get; set; } = false;

        public int Owner { get; set; }

        public ProfileViewModel User { get; set; }

        public CompanyViewModel Company { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public string GetFullName()
        {
            if (this.Anonymous)
            {
                return "Anonymous";
            }

            return this.User.GetFullQualifiedName();
        }
    }
}