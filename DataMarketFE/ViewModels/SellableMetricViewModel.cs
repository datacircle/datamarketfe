﻿namespace DataMarketFE.ViewModels
{
    public class SellableMetricViewModel
    {
        public MetricViewModel Metric { get; set; }

        public MetricTransferViewModel MetricTransfer { get; set; }
    }
}