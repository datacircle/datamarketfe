﻿namespace DataMarketFE.ViewModels.Account
{
    public class MarketplaceSearchUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}