using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Current Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [StringLength(30, ErrorMessage = "The Password must be between 5 and 30 characters", MinimumLength = 5)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$", ErrorMessage = "The password must have at least one UpperCase, one Lowercase, one Numeric and one non-alphanumeric Character.")]
        public string Password { get; set; }

        public string Token { get; set; }
    }
}