using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketFE.ViewModels
{
    public class NewsletterViewModel
    {
        [Column(TypeName = "varchar(100)")]
        public string Email { get; set; }
    }
}