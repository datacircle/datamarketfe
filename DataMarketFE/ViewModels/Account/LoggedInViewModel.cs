using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class LoggedInViewModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public CompanyViewModel Company { get; set; }
    }
}