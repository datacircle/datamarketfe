using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class ProfileViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }    
        [Required]
        public string LastName { get; set; }

        public CompanyViewModel Company { get; set; }

        public string PayPalLink { get; set; }

        public string UnsubscribeToken { get; set; }
        public string DoubleOptInToken { get; set; }
        public bool Subscribed { get; set; }
        public bool DoubleOptedIn { get; set; }

        public string PublicId { get; set; }

        public string GetFullQualifiedName()
        {
            if (this.Company != null)
            {
                return Company.Name;
            }

            return LastName + ", " + FirstName + (Company != null ? " @" + Company.Name : "");
        }
    }
}