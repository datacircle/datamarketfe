﻿using System.Collections.Generic;

namespace DataMarketFE.ViewModels
{
    public class AllCategoriesDrpViewModel
    {
        public IList<CategoryViewModel> Categories { get; set; }
        public string AspForName { get; set; }
        public int SelectedValue { get; set; }
        public string ElementID { get; set; }
    }
}