using System;
using System.Collections.Generic;

namespace DataMarketFE.ViewModels
{
    public class AllDatasourcesDrpViewModel
    {
        public IList<DatasourceViewModel> DatasourceList { get; set; }
        public string AspForName { get; set; }
        public Guid SelectedValue { get; set; }
        public string ElementID { get; set; }
    }
}