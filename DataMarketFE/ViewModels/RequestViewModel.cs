using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class RequestViewModel
    {
        public Guid ID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [StringLength(10000, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Description { get; set; }

        public string DescriptionPlainText { get; set; }

        public int UpvoteCount { get; set; } = 0;

        public int CommentCount { get; set; } = 0;

        public CompanyViewModel Company { get; set; }

        public ProfileViewModel User { get; set; }

        public bool Anonymous { get; set; } = false;

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}