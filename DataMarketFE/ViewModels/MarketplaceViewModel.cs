﻿using System.Collections.Generic;

namespace DataMarketFE.ViewModels
{
    public class MarketplaceViewModel
    {
        public IEnumerable<MarketplaceMetricViewModel> Metrics { get; set; }
        public string SearchTerm { get; set; }
        public string HashTagTerm { get; set; }

        public string GetSearchQuery()
        {
            string returnString = "";

            if (string.IsNullOrEmpty(SearchTerm) && string.IsNullOrEmpty(HashTagTerm))
            {
                returnString = "";
            }
            else if (string.IsNullOrEmpty(SearchTerm))
            {
                returnString = GetHashTagsWithSymbol();
            }
            else if (string.IsNullOrEmpty(HashTagTerm))
            {
                returnString = SearchTerm;
            }
            else
            {
                returnString = SearchTerm + " " + GetHashTagsWithSymbol();
            }            

            return returnString;
        }

        public string GetHashTagsWithSymbol()
        {
            if (this.HashTagTerm == null || string.IsNullOrEmpty(this.HashTagTerm))
            {
                return "";
            }

            if (this.HashTagTerm.Contains("#"))
            {
                return this.HashTagTerm;
            }
            else
            {
                string[] hashtags = this.HashTagTerm.Split(',');

                if (hashtags.Length == 1)
                {
                    return "#" + hashtags[0];
                }

                return string.Join('#', hashtags);
            }
        }

        public string Test()
        {
            return "asdas";
        }
    }
}