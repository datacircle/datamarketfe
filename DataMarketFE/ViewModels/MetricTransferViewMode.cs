﻿using DataMarketFE.Utilities.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMarketFE.ViewModels
{
    public class MetricTransferViewModel
    {
        public enum StatusCode
        {
            Error = 0,

            Created = 10,

            Queued = 11,

            Started = 20,

            Completed = 30
        }

        [Required]
        [CustomAttributeNoGuidEmpty]
        public Guid MetricID { get; set; }

        public Guid TargetDatasourceID { get; set; }

        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 0)]
        public string TargetTableName { get; set; }

        public bool DownloadAsFile { get; set; }

        public int? OriginCompanyID { get; set; }

        [ForeignKey("OriginCompanyID")]
        public CompanyViewModel OriginCompany { get; set; }

        public int? TargetCompanyID { get; set; }

        [ForeignKey("TargetCompanyID")]
        public CompanyViewModel TargetCompany { get; set; }

        public StatusCode Status { get; set; }

        public string Message { get; set; }

        public int ActionableUser { get; set; }

        [ForeignKey("Owner")]
        public ProfileViewModel User { get; set; }
    }
}