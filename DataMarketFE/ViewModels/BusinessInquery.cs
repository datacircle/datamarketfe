using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class BusinessInquery
    {
        
        public bool Buy { get; set; } = true;
        [Required]
        public string Description { get; set; }
        public string Price { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set;}
    }
}