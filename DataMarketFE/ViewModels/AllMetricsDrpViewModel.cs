using System;
using System.Collections.Generic;

namespace DataMarketFE.ViewModels
{
    public class AllMetricsDrpViewModel
    {
        public IEnumerable<MetricViewModel> MetricList { get; set; }
        public string AspForName { get; set; }
        public Guid SelectedValue { get; set; }
    }
}