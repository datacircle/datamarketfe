﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class CommentViewModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(1500, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Value { get; set; }

        public Guid TargetId { get; set; }

        public ProfileViewModel User { get; set; }

        public CommentType Type { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}