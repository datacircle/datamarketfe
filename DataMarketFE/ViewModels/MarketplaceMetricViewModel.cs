﻿using DataMarketFE.ViewModels.Account;
using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class MarketplaceMetricViewModel
    {
        public Guid ID { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public string DescriptionPlainText { get; set; }

        public string Structure { get; set; }

        public MarketplaceSearchUserViewModel User { get; set; }

        public CompanyViewModel Company { get; set; }

        public int Interval { get; set; }

        public bool Open { get; set; } = false;

        public bool Anonymous { get; set; } = false;

        public int Cost { get; set; } = 0;

        public double Rating { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime LastPool { get; set; }

        public string GetFullName()
        {
            if (this.Anonymous)
            {
                return "Anonymous";
            }

            if (this.Company != null)
            {
                return this.Company.Name;
            }

            return this.User.FirstName + "," + this.User.LastName;
        }
    }
}