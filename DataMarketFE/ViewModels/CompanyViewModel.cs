namespace DataMarketFE.ViewModels
{
    public class CompanyViewModel
    {
        public string Name { get; set; }

        public string NormalizedName { get; set; }

        public string Description { get; set; }
    }
}