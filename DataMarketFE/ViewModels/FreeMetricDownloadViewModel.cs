using System;
using System.ComponentModel.DataAnnotations;

namespace DataMarketFE.ViewModels
{
    public class FreeMetricDownloadViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public Guid MetricId { get; set; }
    }
}