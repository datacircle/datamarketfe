﻿using System;

namespace DataMarketFE.Models
{
    public class FileShareModel
    {
        public Guid MetricId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}