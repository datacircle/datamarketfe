﻿using System;
using DataMarketFE.ViewModels;

namespace DataMarketFE.Models
{
    public class MetricAccessMapModel
    {
        public int ID { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public Guid MetricId { get; set; }

        public int? TargetUserId { get; set; }

        public ProfileViewModel TargetUser { get; set; }
    }
}