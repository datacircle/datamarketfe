using ProtoBuf;
using System;
using System.Collections.Generic;

namespace DataMarketFE.Models
{
    [ProtoContract]
    public class DataStoreCollectionModel
    {
        [ProtoMember(1)]
        public Guid ID { get; set; }

        [ProtoMember(2)]
        public List<List<string>> Data { get; set; }

        [ProtoMember(3)]
        public Dictionary<string, string> Structure { get; set; } = new Dictionary<string, string> { };

        [ProtoMember(4)]
        public string FileFullPath { get; set; }
        
        [ProtoMember(5)]
        public string FileName { get; set; }
    }
}