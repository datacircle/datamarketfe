using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FE.ViewComponents
{
    public class UserViewComponent : ViewComponent
    {
        private readonly ILogger _logger;
        private static HttpClient Client = new HttpClient();
        private static string API_PATH = "http://localhost:5000/";

        public UserViewComponent(ILogger<UserViewComponent> logger)
        {
            _logger = logger;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            LoggedInViewModel loggedInViewModel = new LoggedInViewModel();

            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("authToken"));
                HttpResponseMessage result = await Client.GetAsync(API_PATH + "api/account/GetUserProfile");
                loggedInViewModel = JsonConvert.DeserializeObject<LoggedInViewModel>(await result.Content.ReadAsStringAsync());
            }
            catch (HttpRequestException ex)
            {
                _logger.LogError("Error accessing API[" + API_PATH + ": ", ex);
            }

            return View(loggedInViewModel);
        }
    }
}