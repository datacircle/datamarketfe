using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class AllMetricsDrpViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public AllMetricsDrpViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(string aspForName, Guid selectedValue)
        {
            AllMetricsDrpViewModel vm = new AllMetricsDrpViewModel
            {
                MetricList = await _dataMarketApiClient.GetAllMetricsWithinCompany(),
                AspForName = aspForName,
                SelectedValue = selectedValue
            };

            return View(vm);
        }
    }
}