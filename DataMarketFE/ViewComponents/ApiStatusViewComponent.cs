using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class ApiStatusViewComponent : ViewComponent
    {
        private readonly ILogger _logger;
        private static HttpClient Client = new HttpClient();
        private static string API_PATH = "http://localhost:5000/";

        public ApiStatusViewComponent(ILogger<ApiStatusViewComponent> logger)
        {
            _logger = logger;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            ApiStatusViewModel ApiStatus = new ApiStatusViewModel
            {
                Message = "yo"
            };

            try
            {
                HttpResponseMessage result = await Client.GetAsync(API_PATH + "api/status/heartbeat");
                ApiStatus.Status = result.StatusCode == HttpStatusCode.OK;
            }
            catch (HttpRequestException ex)
            {
                _logger.LogError("Error accessing API[" + API_PATH + Environment.NewLine + ex.ToString());
                ApiStatus.Status = false;
            }

            return View(ApiStatus);
        }
    }
}