﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using DataMarketFE.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DataMarketFE.ViewComponents
{
    public class MetricShareViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MetricShareViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid metricId)
        {
            IEnumerable<MetricAccessMapModel> _rcCollection = await _dataMarketApiClient.GetAllShareMetric(metricId);

            if (_rcCollection == null)
            {
                _rcCollection = new List<MetricAccessMapModel>();
            }

            return View(_rcCollection);
        }
    }
}