using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class AllDatasourcesDrpViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public AllDatasourcesDrpViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(string elementID, string aspForName, Guid selectedValue, bool omitSystemSensitiveTypes = true)
        {
            AllDatasourcesDrpViewModel vm = new AllDatasourcesDrpViewModel
            {
                DatasourceList = await _dataMarketApiClient.GetDatasourceAll(),
                AspForName = aspForName,
                SelectedValue = selectedValue,
                ElementID = elementID
            };

            if (omitSystemSensitiveTypes)
            {
                DatasourceViewModel dsUpload = vm.DatasourceList.FirstOrDefault(dsa => dsa.Type == ViewModels.Type.FileUploadGeneric);
                DatasourceViewModel dsDownload = vm.DatasourceList.FirstOrDefault(dsa => dsa.Type == ViewModels.Type.FileDownload);
                DatasourceViewModel dsLink = vm.DatasourceList.FirstOrDefault(dsa => dsa.Type == ViewModels.Type.Link);
                DatasourceViewModel dsAirtable = vm.DatasourceList.FirstOrDefault(dsa => dsa.Type == ViewModels.Type.Airtable);

                vm.DatasourceList.Remove(dsAirtable);            
                vm.DatasourceList.Remove(dsLink);            
                vm.DatasourceList.Remove(dsUpload);
                vm.DatasourceList.Remove(dsDownload);
            }

            return View(vm);
        }
    }
}