using DataMarketFE.APIs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class DatasourcesCardViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public DatasourcesCardViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            return View("", await _dataMarketApiClient.GetDatasourcesCount());
        }
    }
}