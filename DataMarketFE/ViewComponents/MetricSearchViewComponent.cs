﻿using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class MetricSearchViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MetricSearchViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            AllMetricsDrpViewModel vm = new AllMetricsDrpViewModel
            {
                MetricList = await _dataMarketApiClient.GetAllMetricsWithinCompany()
            };

            if (vm.MetricList == null)
            {
                vm.MetricList = new List<MetricViewModel>();
            }

            return View(vm);
        }
    }
}