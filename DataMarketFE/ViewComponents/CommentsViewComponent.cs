using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class CommentsViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public CommentsViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(CommentTypeViewModel vm)
        {
            IEnumerable<CommentViewModel> _rcCollection = null;

            switch (vm.Type)
            {
                case CommentType.Request:
                    _rcCollection = await _dataMarketApiClient.GetAllRequestComments(vm.Id);
                    break;
                case CommentType.Metric:
                    _rcCollection = await _dataMarketApiClient.GetAllMetricComments(vm.Id);
                    break;
            }            

            if (_rcCollection == null)
            {
                _rcCollection = new List<CommentViewModel>();
            }

            return View(_rcCollection);
        }
    }
}