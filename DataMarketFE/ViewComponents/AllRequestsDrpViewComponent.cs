using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class AllRequestsDrpViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public AllRequestsDrpViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(int maxPriority, bool isDone)
        {
            IEnumerable<RequestViewModel> result = await _dataMarketApiClient.GetRequestAll(false);

            return View(result);
        }
    }
}