using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class CategoriesIndexViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public CategoriesIndexViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var a = await _dataMarketApiClient.GetMetricCategoriesAll();
            return View(a);
        }
    }
}