using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class MetricCategoriesDrpViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public MetricCategoriesDrpViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync(string elementID, string aspForName, int selectedValue)
        {
            AllCategoriesDrpViewModel vm = new AllCategoriesDrpViewModel
            {
                Categories = await _dataMarketApiClient.GetMetricCategoriesAll(),
                AspForName = aspForName,
                SelectedValue = selectedValue,
                ElementID = elementID
            };

            return View(vm);
        }
    }
}