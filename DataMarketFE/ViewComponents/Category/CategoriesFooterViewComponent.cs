using DataMarketFE.APIs;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMarketFE.ViewComponents
{
    public class CategoriesFooterViewComponent : ViewComponent
    {
        private readonly DataMarketApiClient _dataMarketApiClient;

        public CategoriesFooterViewComponent(DataMarketApiClient dataMarketApiClient)
        {
            _dataMarketApiClient = dataMarketApiClient;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<CategoryViewModel> _cList = new List<CategoryViewModel>();

            var a = await _dataMarketApiClient.GetMetricCategoriesAll();

            if (a != null)
            {
                _cList = a.Where(c => c.UsageCount > 0).OrderBy(c => c.Name).ToList();
            }

            return View(_cList);
        }
    }
}