using DataMarketFE.Models;
using DataMarketFE.Settings;
using DataMarketFE.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DataMarketFE.APIs
{
    public class DataMarketApiClient
    {
        public readonly ApiEndpoints _apiEndpoints;
        public readonly IHttpContextAccessor _httpContextAccessor;
        public ISession _session;
		private HttpClient _httpClient;

        private HttpClient Client => _httpClient ?? (_httpClient = new HttpClient
        {
            BaseAddress = new Uri(_apiEndpoints.DataMarketApi)
        });
      

        public DataMarketApiClient(IOptions<ApiEndpoints> apiEndpoints, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            _apiEndpoints = apiEndpoints.Value;
        }

        #region "Generic"
        public async Task<HttpResponseMessage> PostAsync(string url, object payload)
        {
            return await Client.PostAsJsonAsync(url, payload);
        }
        #endregion

        #region "Account"

        public async Task<HttpResponseMessage> Register(RegisterViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/Account/Register", vm);
        }

        public async Task<HttpResponseMessage> UpdateProfile(ProfileViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/Account/UpdateProfile", vm, _session.AuthenticationToken());
        }

        public async Task<HttpResponseMessage> RegisterBeta(RequestBetaViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/Account/RequestBeta", vm);
        }

        public async Task<HttpResponseMessage> Login(LoginViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/Account/Login", vm);
        }

        public async Task<ProfileViewModel> GetUserProfile()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/account/GetUserProfile", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return new ProfileViewModel();
            }

            return JsonConvert.DeserializeObject<ProfileViewModel>(await responce.Content.ReadAsStringAsync());
        }

        public Task ForgotPassword(string email)
        {
            throw new NotImplementedException();
        }

        public Task ConfirmEmail(string verificationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<HttpResponseMessage> RequestPasswordReset(string email)
        {
            return await Client.Get(_apiEndpoints.DataMarketApi + "/api/Account/ForgotPassowrd/" + email);
        }

        public async Task<HttpResponseMessage> ResetPassword(ResetPasswordViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/Account/resetpassword", vm, _session.AuthenticationToken());
        }

        #endregion "Account"

        #region "QueryLog"

        public async Task<List<QueryLogViewModel>> GetQueryLog()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/querylog/all", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return new List<QueryLogViewModel>();
            }

            List<QueryLogViewModel> qlVmCollection =
                JsonConvert.DeserializeObject<List<QueryLogViewModel>>(await responce.Content.ReadAsStringAsync());

            return qlVmCollection;
        }

        #endregion "QueryLog"

        #region "Datasource"

        public async Task<IList<DatasourceViewModel>> GetDatasourceAll()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/datasource", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            string a = await responce.Content.ReadAsStringAsync();

            IList<DatasourceViewModel> _datasourceVm =
                JsonConvert.DeserializeObject<IList<DatasourceViewModel>>(a);

            return _datasourceVm;
        }

        public async Task<string> CheckAnonymConnection(DatasourceViewModel vm)
        {
            HttpResponseMessage responce = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/connector", vm, _session.AuthenticationToken());
            return await responce.Content.ReadAsStringAsync();
        }

        public async Task<DatasourceViewModel> GetDatasource(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/datasource/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            DatasourceViewModel _datasourceVm =
                JsonConvert.DeserializeObject<DatasourceViewModel>(await responce.Content.ReadAsStringAsync());

            return _datasourceVm;
        }

        public async Task<DatasourceViewModel> CreateDatasource(DatasourceViewModel vm)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/datasource", vm, _session.AuthenticationToken());

            if (!result.IsSuccessStatusCode)
            {
                return null;
            }

            DatasourceViewModel _datasourceVM =
                JsonConvert.DeserializeObject<DatasourceViewModel>(await result.Content.ReadAsStringAsync());

            return _datasourceVM;
        }

        public async Task<bool> UpdateDatasource(DatasourceViewModel vm)
        {
            HttpResponseMessage result = await Client.Put(_apiEndpoints.DataMarketApi + "/api/datasource/" + vm.ID, vm, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<HttpResponseMessage> DeleteDatasource(Guid id)
        {
            return await Client.Delete(_apiEndpoints.DataMarketApi + "/api/datasource/" + id.ToString(), _session.AuthenticationToken());
        }

        #endregion "Datasource"

        #region "Metric"

        private async Task<IEnumerable<MetricViewModel>> MetricResult(HttpResponseMessage responce)
        {
            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<MetricViewModel> _metricVm =
                JsonConvert.DeserializeObject<IEnumerable<MetricViewModel>>(await responce.Content.ReadAsStringAsync());

            return _metricVm;
        }

        public async Task<bool> UseFile(FileShareModel model)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/storage", model, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<bool> UploadFile(Guid metricId, string fileName, FileStream stream)
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _session.AuthenticationToken());

            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            MultipartFormDataContent form = new MultipartFormDataContent();

            HttpContent content = new StringContent("fileToUpload");
            form.Add(content, "fileToUpload");

            content = new StreamContent(stream);

            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "fileToUpload",
                FileName = fileName
            };
            form.Add(content);

            HttpResponseMessage response = await Client.PostAsync(_apiEndpoints.DataMarketApi + "/api/cloudstorage/" + metricId.ToString(), form);

            //var inputData = new StreamContent(stream);

            //HttpResponseMessage result = await Client.PostAsync(
            //    _apiEndpoints.DataMarketApi + "/api/cloudstorage/" + metricId.ToString(), inputData);

            return true;
        }

        public async Task<IEnumerable<MetricViewModel>> GetAllMetricsForRequest()
        {
            return await MetricResult(await Client.Get(_apiEndpoints.DataMarketApi + "/api/metric/?filterForRequest=true", _session.AuthenticationToken()));
        }

        public async Task<IEnumerable<MetricViewModel>> GetAllMetricsWithinCompany()
        {
            return await MetricResult(await Client.Get(_apiEndpoints.DataMarketApi + "/api/metric/?filterForCompany=true", _session.AuthenticationToken()));
        }

        public async Task<MetricViewModel> GetMetric(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/metric/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            MetricViewModel _metricVm =
                JsonConvert.DeserializeObject<MetricViewModel>(await responce.Content.ReadAsStringAsync());

            return _metricVm;
        }

        public async Task<MetricViewModel> CreateMetric(MetricViewModel vm)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/metric", vm, _session.AuthenticationToken());

            if (!result.IsSuccessStatusCode)
            {
                return null;
            }

            MetricViewModel _metricVm =
                JsonConvert.DeserializeObject<MetricViewModel>(await result.Content.ReadAsStringAsync());

            return _metricVm;
        }

        public async Task<bool> UpdateMetric(MetricViewModel vm)
        {
            HttpResponseMessage result = await Client.Put(_apiEndpoints.DataMarketApi + "/api/metric/" + vm.ID.ToString(), vm, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteMetric(Guid id)
        {
            HttpResponseMessage result = await Client.Delete(_apiEndpoints.DataMarketApi + "/api/metric/" + id.ToString(), _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<DataStoreCollectionModel> GetData(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/data/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            var content = await responce.Content.ReadAsStringAsync();

            if (content == null)
            {
                return new DataStoreCollectionModel();
            }

            return JsonConvert.DeserializeObject<DataStoreCollectionModel>(content);
        }

        public async Task<HttpStatusCode> TriggerDataPool(Guid id)
        {
            return (await Client.Get(_apiEndpoints.DataMarketApi + "/api/data/trigger/" + id.ToString(), _session.AuthenticationToken())).StatusCode;
        }

        #endregion "Metric"

        #region "Request"

        public async Task<RequestViewModel> GetRequest(Guid id, bool filterForAll = false)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/request/" + id.ToString() + "/?filterForAll=" + filterForAll.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            RequestViewModel _requestVm =
                JsonConvert.DeserializeObject<RequestViewModel>(await responce.Content.ReadAsStringAsync());

            return _requestVm;
        }

        public async Task<IEnumerable<RequestViewModel>> GetRequestAll(bool filterForAll = false)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/request/?filterForAll=" + filterForAll.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<RequestViewModel> _requestVm =
                JsonConvert.DeserializeObject<IEnumerable<RequestViewModel>>(await responce.Content.ReadAsStringAsync());

            return _requestVm;
        }

        public async Task<RequestViewModel> CreateRequest(RequestViewModel vm)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/request", vm, _session.AuthenticationToken());

            if (!result.IsSuccessStatusCode)
            {
                return null;
            }

            RequestViewModel _requestVm =
                JsonConvert.DeserializeObject<RequestViewModel>(await result.Content.ReadAsStringAsync());

            return _requestVm;
        }

        public async Task<bool> UpdateRequest(RequestViewModel vm)
        {
            HttpResponseMessage result = await Client.Put(_apiEndpoints.DataMarketApi + "/api/request/" + vm.ID, vm, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteRequest(Guid id)
        {
            HttpResponseMessage result = await Client.Delete(_apiEndpoints.DataMarketApi + "/api/request/" + id.ToString(), _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        #endregion "Request"

        #region "RequestComment"

        public async Task<bool> PostRequestComment(CommentViewModel vm)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/requestcomment", vm, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteRequestComment(int id)
        {
            HttpResponseMessage result = await Client.Delete(_apiEndpoints.DataMarketApi + "/api/requestcomment/" + id.ToString(), _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<CommentViewModel>> GetAllRequestComments(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/requestcomment/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<CommentViewModel> _requestVm =
                JsonConvert.DeserializeObject<IEnumerable<CommentViewModel>>(await responce.Content.ReadAsStringAsync());

            return _requestVm;
        }

        #endregion "RequestComment"

        #region "MetricComment"

        public async Task<bool> PostMetricComment(CommentViewModel vm)
        {
            HttpResponseMessage result = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/metriccomment", vm, _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteMetricComment(int id)
        {
            HttpResponseMessage result = await Client.Delete(_apiEndpoints.DataMarketApi + "/api/metriccomment/" + id.ToString(), _session.AuthenticationToken());
            return result.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<CommentViewModel>> GetAllMetricComments(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/metriccomment/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<CommentViewModel> _requestVm =
                JsonConvert.DeserializeObject<IEnumerable<CommentViewModel>>(await responce.Content.ReadAsStringAsync());

            return _requestVm;
        }

        #endregion "MetricComment"

        #region "RequestVote"

        public async Task<bool> Upvote(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/RequestVote/" + id.ToString() + "?upvote=true", _session.AuthenticationToken());

            return responce.IsSuccessStatusCode;
        }

        public async Task<bool> Downvote(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/RequestVote/" + id.ToString() + "?downvote=true", _session.AuthenticationToken());

            return responce.IsSuccessStatusCode;
        }

        #endregion "RequestVote"

        #region "Cards"

        public async Task<string> GetConsumerCount()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/cards/consumercount", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return "0";
            }

            return await responce.Content.ReadAsStringAsync();
        }

        public async Task<string> GetDatasourcesCount()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/cards/datasourcecount", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return "0";
            }

            return await responce.Content.ReadAsStringAsync();
        }

        public async Task<string> GetMetricsCount()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/cards/metriccount", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return "0";
            }

            return await responce.Content.ReadAsStringAsync();
        }

        public async Task<string> GetRequestsCount()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/cards/requestcount", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return "0";
            }

            return await responce.Content.ReadAsStringAsync();
        }

        #endregion "Cards"

        #region "Marketplace"

        public async Task<IEnumerable<MetricViewModel>> GetMarketplaceAll()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/marketplace", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<MetricViewModel> _metricVm =
                JsonConvert.DeserializeObject<IEnumerable<MetricViewModel>>(await responce.Content.ReadAsStringAsync());

            return _metricVm;
        }

        public async Task<MetricViewModel> GetMetricForMarketplace(Guid id)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/marketplace/" + id.ToString(), _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            MetricViewModel _metricVm =
                JsonConvert.DeserializeObject<MetricViewModel>(await responce.Content.ReadAsStringAsync());

            return _metricVm;
        }

        #endregion "Marketplace"

        #region "Rating"

        public async Task<string> CreateRating(RatingViewModel vm)
        {
            HttpResponseMessage response = await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/rating", vm, _session.AuthenticationToken());
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return string.Empty;
        }

        public async Task<string> UpdateRating(RatingViewModel vm)
        {
            HttpResponseMessage response = await Client.Put(_apiEndpoints.DataMarketApi + "/api/rating/" + vm.ID, vm, _session.AuthenticationToken());
            if (response.IsSuccessStatusCode)
            {
                return response.ToString();
            }

            return string.Empty;
        }

        public async Task<string> DeleteRating(int id)
        {
            HttpResponseMessage response = await Client.Delete(_apiEndpoints.DataMarketApi + "/api/ratings/" + id.ToString(), _session.AuthenticationToken());
            if (response.IsSuccessStatusCode)
            {
                return response.ToString();
            }

            return string.Empty;
        }

        #endregion "Rating"

        #region MetricTransfer

        public async Task<HttpResponseMessage> MetricTransfer(MetricTransferViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/metricTransfer", vm, _session.AuthenticationToken());
        }

        public async Task<IEnumerable<MetricTransferViewModel>> GetAllMetricTransfers()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/metricTransfer", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<MetricTransferViewModel> _metricTransferVm =
                JsonConvert.DeserializeObject<IEnumerable<MetricTransferViewModel>>(await responce.Content.ReadAsStringAsync());

            return _metricTransferVm;
        }

        #endregion MetricTransfer

        #region NonUserDownloadMetric
        public async Task<HttpResponseMessage> RequestFreeMetricDownload(FreeMetricDownloadViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/NonUserDownloadMetric/Create", vm, _session.AuthenticationToken());
        }

        public async Task<HttpResponseMessage> JustAddEmail(FreeMetricDownloadViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/NonUserDownloadMetric/justadd", vm, _session.AuthenticationToken());
        }

        public async Task<HttpResponseMessage> DownloadFreeMetric(Guid metricDownloadId)
        {
            return await Client.Get(_apiEndpoints.DataMarketApi + "/api/NonUserDownloadMetric/Download/" + metricDownloadId.ToString(), _session.AuthenticationToken());
        }
        #endregion NonUserDownloadMetric

        #region MetricShare
        public async Task<IEnumerable<MetricAccessMapModel>> GetAllShareMetric(Guid metricId)
        {
            HttpResponseMessage response = await Client.Get(_apiEndpoints.DataMarketApi + "/api/MetricAccess/" + metricId.ToString(), _session.AuthenticationToken());

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<MetricAccessMapModel> _collection =
                JsonConvert.DeserializeObject<IEnumerable<MetricAccessMapModel>>(await response.Content.ReadAsStringAsync());

            return _collection;
        }

        public async Task<HttpResponseMessage> ShareMetric(MetricAccessViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/MetricAccess", vm, _session.AuthenticationToken());
        }

        public async Task<HttpResponseMessage> DeleteShareMetric(int metricAccessId)
        {
            return await Client.Delete(_apiEndpoints.DataMarketApi + "/api/MetricAccess/" + metricAccessId.ToString(), _session.AuthenticationToken());
        }
        #endregion MetricShare

        #region Search

        public async Task<HttpResponseMessage> SearchMetric(string SearchTerm, string HashTagTerm)
        {
            var parameters = "?searchTerm=" + WebUtility.UrlEncode(SearchTerm) + "&hashtagTerm=" + HashTagTerm;

            return await Client.Get(_apiEndpoints.DataMarketApi + $"/api/MetricSearch/{parameters}", _session.AuthenticationToken());
        }

        public async Task<HttpResponseMessage> SearchHashTag(string SearchTerm)
        {
            return await Client.Get(_apiEndpoints.DataMarketApi + $"/api/hashtag/{SearchTerm}", _session.AuthenticationToken());
        }
        #endregion Search

        #region MetricCategories 
        public async Task<IList<CategoryViewModel>> GetMetricCategoriesAll()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/category", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            string a = await responce.Content.ReadAsStringAsync();

            IList<CategoryViewModel> _metricCategoriesViewModel =
                JsonConvert.DeserializeObject<IList<CategoryViewModel>>(a);

            return _metricCategoriesViewModel;
        }

        public async Task<CategoryViewModel> GetMetricCategory(string categoryName)
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/category/" + categoryName, _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            string a = await responce.Content.ReadAsStringAsync();

            CategoryViewModel _metricCategoriesViewModel =
                JsonConvert.DeserializeObject<CategoryViewModel>(a);

            return _metricCategoriesViewModel;
        }
        #endregion MetricCategories

        #region Order    
        public async Task<HttpResponseMessage> Order(OrderViewModel vm)
        {
            return await Client.PostAsJsonAsync(_apiEndpoints.DataMarketApi + "/api/order", vm, _session.AuthenticationToken());
        }

        public async Task<OrderViewModel> GetOrderStatus(Guid metricId)
        {
            HttpResponseMessage response = await Client.Get(_apiEndpoints.DataMarketApi + "/api/order/" + metricId.ToString(), _session.AuthenticationToken());

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            OrderViewModel order =
                JsonConvert.DeserializeObject<OrderViewModel>(await response.Content.ReadAsStringAsync());

            return order;
        }

        public async Task<IEnumerable<OrderViewModel>> GetAllOrders()
        {
            HttpResponseMessage responce = await Client.Get(_apiEndpoints.DataMarketApi + "/api/order/", _session.AuthenticationToken());

            if (!responce.IsSuccessStatusCode)
            {
                return null;
            }

            IEnumerable<OrderViewModel> _orderCollection =
                JsonConvert.DeserializeObject<IEnumerable<OrderViewModel>>(await responce.Content.ReadAsStringAsync());

            return _orderCollection;
        }
        #endregion
    }
}