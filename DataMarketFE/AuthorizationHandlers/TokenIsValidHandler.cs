using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace DataMarketFE.AuthorizationHandler
{
    public class TokenIsValidHandler : AuthorizationHandler<TokenValidityRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TokenValidityRequirement requirement)
        {
            var mvcContext = context.Resource as Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext;

            if (mvcContext.HttpContext.Session != null)
            {
                string token = mvcContext.HttpContext.Session.AuthenticationToken();

                if (token == null || token.Length == 0)
                {
                    // context.Fail();
                    return Task.CompletedTask;
                }

                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                SecurityToken securityToken = handler.ReadToken(token);

                if (securityToken.ValidTo > DateTime.UtcNow)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    // context.Fail();
                }
            }
            else
            {
                // context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}