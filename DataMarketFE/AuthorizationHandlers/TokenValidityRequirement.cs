using Microsoft.AspNetCore.Authorization;

namespace DataMarketFE.AuthorizationHandler
{
    public class TokenValidityRequirement : IAuthorizationRequirement
    {
        public TokenValidityRequirement(bool value)
        {
            Value = value;
        }

        protected bool Value { get; set; }
    }
}